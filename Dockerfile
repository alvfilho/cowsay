FROM debian
LABEL maintainer="alvfilho"

RUN apt-get update && apt-get install -y \
    cowsay \
    fortunes \
    fortunes-br

COPY entrypoint.sh /

ENTRYPOINT [ "/entrypoint.sh" ]